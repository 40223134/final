import os
import sys
import cherrypy
import random
import math

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)

# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "/local_data/"
    data_dir = _curdir + "/local_data/"
#@-<<declarations>>
#@+others
#@+node:ppython.20131221094631.1657: ** class Guess
class Guess(object):
    # 登入驗證後必須利用 session 機制儲存
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8',
    # 加入 session 設定
    'tools.sessions.on' : True,
    'tools.sessions.storage_type' : 'file',
    'tools.sessions.locking' : 'explicit',
    # 就 OpenShift ./tmp 位於 app-root/runtime/repo/tmp
    # tmp 目錄與 wsgi 目錄同級
    'tools.sessions.storage_path' : data_dir+'tmp',
    # 內定的 session timeout 時間為 60 分鐘
    'tools.sessions.timeout' : 60
    }

    #@+others
    #@+node:ppython.20131221094631.1658: *3* index
    @cherrypy.expose
    def index(self, guess=None):
        # 將標準答案存入 answer session 對應區
        h = None
        t = h/4.9
    outsring = (math.sqrt(t))
        超文件輸出 = "<form method=POST action=doCheck>"
        超文件輸出 += "樓層高度:<input type=text name=guess><br \>"
        超文件輸出 += "<input type=submit value=send>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        return 超文件輸出

#@-others
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(Guess(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(Guess(), config = application_conf)
#@-leo
